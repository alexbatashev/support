create table employees
(
  id       int auto_increment
    primary key,
  fio      varchar(255)         null,
  is_admin tinyint(1) default 0 null,
  login    varchar(255)         null,
  pass     varchar(255)         null
);

INSERT INTO support.employees (id, fio, is_admin, login, pass) VALUES (1, 'adsfaf', 1, 'admin', 'pass');
INSERT INTO support.employees (id, fio, is_admin, login, pass) VALUES (2, 'Empl', 0, 'empl', 'pass');
create table sessions
(
  session_id varchar(128) collate utf8mb4_bin not null
    primary key,
  expires    int(11) unsigned                 not null,
  data       text collate utf8mb4_bin         null
);

INSERT INTO support.sessions (session_id, expires, data) VALUES ('4LVYJWcUo9B92bh_BE4q1dIlqA-8DnDj', 1545564145, '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"}}');
INSERT INTO support.sessions (session_id, expires, data) VALUES ('SFJ_EeDg9U_4-15bijUhB6px0W8q3Mz1', 1545566358, '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"uid":1,"isEmployee":true,"isAdmin":1}');
INSERT INTO support.sessions (session_id, expires, data) VALUES ('o8bhTrdw1lSP32vZCxQip7UeHVDifyF1', 1545564145, '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"}}');
create table tickets
(
  id       int auto_increment
    primary key,
  problem  text          null,
  assignee int           null,
  reporter int           null,
  status   int default 0 null,
  constraint tickets_employees_id_fk
    foreign key (assignee) references employees (id),
  constraint tickets_users_id_fk
    foreign key (reporter) references users (id)
);

INSERT INTO support.tickets (id, problem, assignee, reporter, status) VALUES (1, 'adsfasdfja;sdfa;lsdfds;kflasx', null, 1, 1);
create table users
(
  id    int auto_increment
    primary key,
  login varchar(255) null,
  pass  varchar(255) null,
  fio   varchar(255) null,
  phone varchar(255) null
);

INSERT INTO support.users (id, login, pass, fio, phone) VALUES (1, 'login', 'pass', 'adfada', '123');