var express = require('express');
var router = express.Router();
var db = require('../db');

/* GET users listing. */
router.get('/login', function(req, res, next) {
    res.render('employee_login');
});

router.post('/login', function (req, res, next) {
    db.query('SELECT * FROM `employees` WHERE login = "' + req.body.login + '" and pass = "' + req.body.password + '"', function (err, users, f) {
        if (err) {
            console.log(err);
            res.status(500);
            res.end();
        } else {
            if (users.length > 0) {
                req.session.uid = users[0].id;
                req.session.isEmployee = true;
                req.session.isAdmin = users[0].is_admin;
                res.redirect('/');
            } else {
                res.redirect('/employees/login');
            }
        }
    });
});

router.get('/assigned', function (req, res, next) {
    db.query('SELECT * FROM tickets WHERE assignee = ' +req.session.uid, function (err, results, f) {
        if (err) {
            res.status(500);
            res.end();
        } else {
            res.render('all_tickets.hbs', {'tickets': results});
        }
    });
});

router.get('/assign', function(req, res, next) {
    db.query('SELECT * FROM tickets WHERE status = 0', function (err, results, f) {
        if (err) {
            res.status(500);
            res.end();
        } else {
            console.log("boom");
            res.render('assign', {'admin': true, 'tickets': results});
        }
    });
});

router.get('/:id/assign', function (req, res, next) {
    res.render('assign_form', {'id': req.params.id});
});

router.post('/:id/assign', function (req, res, next) {
    db.query('UPDATE tickets SET status = 1 WHERE id = ' + req.params.id, function (err, results, f) {
        if (err) {
            res.send(500);
            res.end();
        } else {
            res.redirect('/employees/assign');
        }
    });
});

module.exports = router;
