var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if (req.session.uid) {
      if (req.session.isEmployee) {
          res.redirect('/employees/assigned');
      } else {
          res.redirect('/task');
      }
  } else {
      res.redirect('/users/login');
  }
});

module.exports = router;
