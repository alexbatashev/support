var express = require('express');
var router = express.Router();
var db = require('../db');


router.get('/', function(req, res, next) {
    res.redirect('/task/new');
});

router.get('/new', function(req, res, next) {
    res.render('new_task');
});

router.post('/new', function (req, res, next) {
    console.log(req.body);
    db.query('INSERT INTO tickets (problem, reporter) VALUES ("'+ req.body.complain +'", ' + req.session.uid + ');' , function(err, results, fields) {
        console.log("query done");
        if (err) {
            console.log(err);
            res.status(500);
            res.end();
        } else {
            res.redirect('/task/my')
        }
    });
});

router.get("/my", function(req, res, next) {
    db.query('SELECT * FROM tickets WHERE reporter = ' +req.session.uid, function (err, results, f) {
        if (err) {
            res.status(500);
            res.end();
        } else {
            res.render('all_tickets.hbs', {'tickets': results});
        }
    });
});

module.exports = router;
